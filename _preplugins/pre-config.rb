require "yaml"
require "set"

class Functions
    def self.compress_images(site)
        dest = Dir.pwd + "/" + site["destination"]

        for collection in site['collections']
            collection_name = collection[0]
            collection_permalink = collection[1]['permalink']
            collection_folder = collection[1]['folder']

            compressedSet = Set.new

            images_list = Dir.glob("#{Dir.pwd+"/"+collection_folder}/**/*.jpg")
            images_list = images_list.select{ |s|
                if s.include? ".c."
                    x = s
                    x = x.sub(".c.", ".")
                    compressedSet.add(x)
                end
                !s.include? ".c."
            }
            images_list = images_list.select{ |s| !compressedSet.include? s }
            
            counter = 0
            for image_url in images_list
                counter+=1
                command = "./_preplugins/image-compress.sh '#{image_url}'"
                system command
                print "Compressing new images: #{counter}/#{images_list.size}\n"
            end
        end
    end
end

config = YAML.load_file("_config.yml")
Functions.compress_images(config)
