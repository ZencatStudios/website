---
layout: collection
title:  "Amsterdam"
subtitle: "Winter & Fall"
date:   2019-07-06 10:42:18 +0530
cover: "/images/home/travel-card1.jpg"
months:
  - Mar
  - Sept
photos: 1238
author: Zencat
---


## Winter
##### October 2019
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Neque aliquam vestibulum morbi blandit cursus risus at. Viverra nibh cras pulvinar mattis nunc sed blandit libero.

<div class="image-container">

![](img1.jpg){:class="image"}
###### Shot on iPhone 11 Pro in Night mode (Wide lens). Guilin, China.
![](photoset1.jpg){:class="image"}
###### Shot on iPhone 11 Pro in Night mode (Wide lens). Guilin, China.
![](photoset2.jpg){:class="image"}
###### Shot on iPhone 11 Pro in Night mode (Wide lens). Guilin, China.
![](photoset3.jpg){:class="image"}
###### Shot on iPhone 11 Pro in Night mode (Wide lens). Guilin, China.
![](photoset4.jpg){:class="image"}
###### Shot on iPhone 11 Pro in Night mode (Wide lens). Guilin, China.

</div>

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Neque aliquam vestibulum morbi blandit cursus risus at. Viverra nibh cras pulvinar mattis nunc sed blandit libero.

<div class="image-container">

![](photoset5.jpg){:class="image"}
###### Shot on iPhone 11 Pro in Night mode (Wide lens). Guilin, China.
![](photoset6.jpg){:class="image"}
###### Shot on iPhone 11 Pro in Night mode (Wide lens). Guilin, China.
![](photoset7.jpg){:class="image"}
###### Shot on iPhone 11 Pro in Night mode (Wide lens). Guilin, China.
<div class="image-row-2">
<div class="caption-container">

![](photoset11.jpg){:class="image"}
###### Shot on iPhone 11 Pro in Night mode (Wide lens). Guilin, China.
</div>
<div class="caption-container">

![](photoset10.jpg){:class="image"}
###### Shot on iPhone 11 Pro in Night mode (Wide lens). Guilin, China.
</div>
</div>

![](photoset8.jpg){:class="image"}
###### Shot on iPhone 11 Pro in Night mode (Wide lens). Guilin, China.
![](photoset9.jpg){:class="image"}
###### Shot on iPhone 11 Pro in Night mode (Wide lens). Guilin, China.

</div>

<div class="fun-fact">

#### FUN FACT
![](flag.jpg){:class="flag-image"}
 Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Nec ullamcorper sit amet risus adipiscing eget felis eget. Natoque penatibus et magnis dis tempor parturient montes. Lorem ipsum dolor sit amet.

</div>

<div class="image-container">

![](photoset15.jpg){:class="image"}
###### Shot on iPhone 11 Pro in Night mode (Wide lens). Guilin, China.
![](photoset16.jpg){:class="image"}
###### Shot on iPhone 11 Pro in Night mode (Wide lens). Guilin, China.
![](photoset17.jpg){:class="image"}
###### Shot on iPhone 11 Pro in Night mode (Wide lens). Guilin, China.
![](photoset18.jpg){:class="image"}
###### Shot on iPhone 11 Pro in Night mode (Wide lens). Guilin, China.
![](photoset19.jpg){:class="image"}
###### Shot on iPhone 11 Pro in Night mode (Wide lens). Guilin, China.

</div>

    
### India
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Neque aliquam vestibulum morbi blandit cursus risus at. Viverra nibh cras pulvinar mattis nunc sed blandit libero.

<div class="image-container">

![](photoset10.jpg){:class="image"}
###### Shot on iPhone 11 Pro in Night mode (Wide lens). Guilin, China.
![](photoset11.jpg){:class="image"}
###### Shot on iPhone 11 Pro in Night mode (Wide lens). Guilin, China.
![](photoset12.jpg){:class="image"}
###### Shot on iPhone 11 Pro in Night mode (Wide lens). Guilin, China.
<div class="image-row-4">

![](photoset11.jpg){:class="image"}
![](photoset10.jpg){:class="image"}
![](photoset16.jpg){:class="image"}
![](photoset14.jpg){:class="image"}

</div>
<div class="image-row-2">
<div class="caption-container">

![](photoset11.jpg){:class="image"}
###### Shot on iPhone 11 Pro in Night mode (Wide lens). Guilin, China.
</div>
<div class="caption-container">

![](photoset10.jpg){:class="image"}
###### Shot on iPhone 11 Pro in Night mode (Wide lens). Guilin, China.
</div>
</div>

![](photoset13.jpg){:class="image"}
###### Shot on iPhone 11 Pro in Night mode (Wide lens). Guilin, China.
![](photoset14.jpg){:class="image"}
###### Shot on iPhone 11 Pro in Night mode (Wide lens). Guilin, China.

</div>





