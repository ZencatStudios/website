module Jekyll
    class CategoryPageGenerator < Generator
      safe true
  
      def generate(site)
        if site.layouts.key? 'categories'
            # if category page exists
            for collection in site.config['collections']
                # for all collections
                name = collection[0]
                if collection[1]["category_pages"]
                    # if this collection wants category pages
                    dir = collection[1]['category_dir'] || 'categories'
                    for category in site.data["collections"][name]
                        docs = site.collections[name].docs.select{ |d|
                            d["category"] == category["title"]
                        }
                        site.pages << CategoryPage.new(site, site.source, File.join(dir, category["title"]), category, docs)
                    end
                end
            end
        #   site.categories.each_key do |category|
        #     site.pages << CategoryPage.new(site, site.source, File.join(dir, category), category)
        #   end
        end
      end
    end
  
    # A Page subclass used in the `CategoryPageGenerator`
    class CategoryPage < Page
      def initialize(site, base, dir, category, docs)
        @site = site
        @base = base
        @dir  = dir
        @name = 'index.html'
  
        self.process(@name)
        self.read_yaml(File.join(base, '_layouts'), 'categories.html')
        self.data['category'] = category
        self.data['docs'] = docs
      end
    end
  end